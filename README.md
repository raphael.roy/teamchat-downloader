Forked from https://github.com/mueller-physics/rocketchat-history-downloader

# Export and Download Rocket.Chat Logs and History

The rocketchat-history-downloader downloads and stores raw JSON channel history for joined channels, private groups, and direct messages. It keeps track of what has already been downloaded and supports ongoing periodic retrieval of new history.

Stores one JSON history file per channel per day in a directory of your choice.

## Getting Started

### Prerequisites

* Python >= 3.5
  * [Pipenv](https://github.com/pypa/pipenv), ideally
  * [rocketchat_API](https://github.com/jadolg/rocketchat_API) Python API wrapper for Rocket.Chat 
  * [py2exe](http://py2exe.org/) Python to executable
* A user account on a [Rocket.Chat](https://rocket.chat/) server that exposes its REST API

### Installing

Install requirements from Pipfile (assuming you have [Pipenv](https://github.com/pypa/pipenv) installed already)

    $ pipenv install
    
Pack script to application.

	$ pipenv run python pack_application.py py2exe

## Usage

### GUI

Execute dist/start_download.exe and fill in your API-User ID and API-Token and name of the channel you want to download. 
If all goes well a .html file with the name of the channel should appear inside the folder dist/history-files.

### console

(Might not work anymore)

	$ xcopy settings-example settings
	$ xcopy history-files-example history-files
	
Fill in appropriate settings in settings/settings.cfg.
	
	$ pipenv run python export.py settings/settings.cfg
	$ pipenv run python html_convert.py settings/settings.cfg name_of_the_channel_to_convert
	
### Logging

By default, debug and info output from any execution is written to the console and logged to a file called export-history.log
