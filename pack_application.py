from distutils.core import setup
from shutil import make_archive, copytree, rmtree
from os import path
import py2exe


setup(console=['start_download.py'])

if path.exists('settings-example'):
    if path.exists('dist/settings'):
        rmtree('dist/settings')
    copytree('settings-example', 'dist/settings')
if path.exists('settings-example'):
    if path.exists('dist/history-files'):
        rmtree('dist/history-files')
    copytree('history-files-example', 'dist/history-files')
make_archive('teamchat-downloader', 'zip', 'dist')