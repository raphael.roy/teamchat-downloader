from distutils.core import setup
from shutil import make_archive, copytree, rmtree
from os import path
import py2exe

# alte Version löschen
rmtree('dist')

# Ausführbare Datei packen
setup(console=['start_download.py'])

if path.exists('dist'):
    if path.exists('settings-example'):
        copytree('settings-example', 'dist/settings')

    if path.exists('history-files-example'):
        copytree('history-files-example', 'dist/history-files')
    make_archive('teamchat-downloader', 'zip', 'dist')