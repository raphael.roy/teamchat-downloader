import tkinter as tk
import configparser
import export as exporter
import html_convert as html_convert



window = tk.Tk()
window.title("Teamchat History Downloader")

channelListBox = tk.Listbox(window, selectmode = 'multiple')

class Config():
    def __init__(self, configfile, datestart, dateend, readonlystate, list):
        self.configfile = configfile
        self.datestart = datestart if datestart != "" else "2000-01-01"
        self.dateend = dateend
        self.readonlystate = readonlystate
        self.list = list

class ConvertConfig():
    def __init__(self, config, channel):
        self.config = config
        self.channel = channel

def download(uidVar, tokenVar, timeVar1, timeVar2, channelVar, imsUsernameVar):
    config_main = configparser.ConfigParser()
    config_main.read('./settings/settings.cfg')
    config_main['rc-api']['user'] = uidVar.get()
    config_main['rc-api']['pass'] = tokenVar.get()
    config_main['rooms']['include'] = '["' + channelVar.get() + '"]'
    config_main['rooms']['ims_ownname'] = imsUsernameVar.get() if imsUsernameVar.get() != "" else None
    with open('./settings/settings.cfg', 'w') as configfile:
        config_main.write(configfile)
    args = Config("./settings/settings.cfg", timeVar1.get(), timeVar2.get(), False, channelVar.get())
    exporter.main(args)

    args = ConvertConfig("./settings/settings.cfg", channelVar.get())
    html_convert.main(args)

uidLabel = tk.Label(window, text = "User ID").grid(row = 0)
uidVar = tk.StringVar(window, value="15-20 Zeichen lange User ID")
uidEntry = tk.Entry(window, textvariable = uidVar).grid(row = 0, column = 1)

tokenLabel = tk.Label(window, text = "API-token").grid(row = 1)
tokenVar = tk.StringVar(window, value="ca 45 Zeichen langer API-Token")
tokenEntry = tk.Entry(window, textvariable = tokenVar).grid(row = 1, column = 1)

timeLabel1 = tk.Label(window, text = "Von (z.B. 2021-01-01)").grid(row = 2)
timeVar1 = tk.StringVar(window, value="")
timeEntry1 = tk.Entry(window, textvariable = timeVar1).grid(row = 2, column = 1)

timeLabel2 = tk.Label(window, text = "Bis (z.B. 2021-03-01)").grid(row = 3)
timeVar2 = tk.StringVar(window, value="")
timeEntry2 = tk.Entry(window, textvariable = timeVar2).grid(row = 3, column = 1)

channelLabel = tk.Label(window, text = "Name des Kanals").grid(row = 4)
channelVar = tk.StringVar(window, value="")
channelEntry = tk.Entry(window, textvariable = channelVar).grid(row = 4, column = 1)

imsUsernameLabel = tk.Label(window, text = "Dein User-Name").grid(row = 5)
imsUsernameVar = tk.StringVar(window, value="")
imsUsernameEntry = tk.Entry(window, textvariable = imsUsernameVar).grid(row = 5, column = 1)

downloadButton = tk.Button(window, text = "Export starten", command = lambda: download(uidVar, tokenVar, timeVar1, timeVar2, channelVar, imsUsernameVar)).grid(row = 6)

window.mainloop()